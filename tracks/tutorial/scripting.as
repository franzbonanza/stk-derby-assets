void onStart()
{
    Utils::logInfo("onStart(): start Driving Tutorial");
    switch (GUI::getRaceGUIType())
    {
        case GUI::KEYBOARD_GAMEPAD:
            GUI::displayStaticMessage(
                GUI::translate("Accelerate with <%s>, and steer with <%s> and <%s>.\nComplete the track by following the minimap.",
                    GUI::getKeyBinding(GUI::PlayerAction::ACCEL),
                    GUI::getKeyBinding(GUI::PlayerAction::STEER_LEFT),
                    GUI::getKeyBinding(GUI::PlayerAction::STEER_RIGHT)
                )
            );
            break;
        case GUI::STEERING_WHEEL:
            GUI::displayStaticMessage(
                GUI::translate("Accelerate by touching the upper part of the wheel, and steer by moving left or right.")
            );
            break;
        case GUI::ACCELEROMETER:
            GUI::displayStaticMessage(
                GUI::translate("Accelerate by moving the accelerator upwards, and steer by tilting your device.")
            );
            break;
        case GUI::GYROSCOPE:
            GUI::displayStaticMessage(
                GUI::translate("Accelerate by moving the accelerator upwards, and steer by rotating your device.")
            );
            break;
        }
}

void onBoxHitByItem(int itemType, int idKart, const string objID)
{
    // Test
    Utils::logInfo("ScriptingCallback: onBoxHitByItem. Item = " + itemType + "; idKart = " + idKart + "; objID = " + objID);
}

void onKartKartCollision(int idKart1, int idKart2)
{
    // Test
    Utils::logInfo("ScriptingCallback: onKartKartCollision: " + idKart1 + " - " + idKart2);
}
void tutorial_hitthebox(int idKart)
{
    Utils::logInfo("tutorial_hitthebox(Kart " + idKart + "): Triggered");
    GUI::displayStaticMessage(
        GUI::translate("Hit the box in front of you and throw it out of the map!!"),
    GUI::ERROR);
}

void tutorial_hitthecar(int idKart)
{
    Utils::logInfo("tutorial_hitthecar(Kart " + idKart + "): Triggered");
    GUI::displayStaticMessage(
        GUI::translate("Now hit the car wreckage too!!"),
    GUI::ERROR);
}

void tutorial_redcircle(int idKart)
{
    Utils::logInfo("tutorial_redcircle(Kart " + idKart + "): Triggered");
        GUI::displayStaticMessage(
            GUI::translate("See the red circle up there? I'll save you the fun for when you play a real match..")
        );
}

void tutorial_handbrake(int idKart)
{
    Utils::logInfo("tutorial_skidding(Kart " + idKart + "): Triggered");
    if (GUI::getRaceGUIType() == GUI::KEYBOARD_GAMEPAD)
    {
        GUI::displayStaticMessage(
            GUI::translate("Accelerate and press the handbrake <%s> key while turning to skid.\nDrifting can help you turn faster to take sharp turns. If you press it without turning,you can brake pretty fast.",
                GUI::getKeyBinding(GUI::PlayerAction::DRIFT)
            )
        );
    }
    else
    {
        GUI::displayStaticMessage(
            GUI::translate("Accelerate and press the skid icon while turning to skid.\nSkidding for a short while can help you turn faster to take sharp turns.",
                GUI::getKeyBinding(GUI::PlayerAction::DRIFT)
            )
        );
    }
}

void tutorial_message1(int idKart)
{
    Utils::logInfo("tutorial_message1(Kart " + idKart + "): Triggered");
        GUI::displayStaticMessage(
            GUI::translate("Remember that in a real match you will lose HPs when hitting other cars. Those Powerups in the red circle could be handy.."));
}


void tutorial_message2(int idKart)
{
    Utils::logInfo("tutorial_backgiftboxes(Kart " + idKart + "): Triggered");
    if (GUI::getRaceGUIType() == GUI::KEYBOARD_GAMEPAD)
    {
        GUI::displayStaticMessage(
            GUI::translate("Press <%s> to look behind.\n You can press LMB (or right gamepad stick) and move the mouse " +
	    "to rotate the camera. Press <%s> to reset it.",
                GUI::getKeyBinding(GUI::PlayerAction::LOOK_BACK),
                GUI::getKeyBinding(GUI::PlayerAction::RESET_CAMERA)
            )
        );
    }
    else
    {
        GUI::displayStaticMessage(
            GUI::translate("Press the mirror icon to look behind.")
        );
    }
}


void tutorial_endmessage(int idKart)
{
    Utils::logInfo("tutorial_endmessage(Kart " + idKart + "): Triggered");
    GUI::displayMessage(
        GUI::translate("You are now ready for Derby Destruction. Have Fun!"),
    GUI::ACHIEVEMENT);
}

void tutorial_exit(int idKart)
{
    Utils::logInfo("tutorial_exit(Kart " + idKart + "): Tutorial quitted");
    Track::exitRace();
}

void tutorial_discard(int idKart)
{
    Utils::logInfo("tutorial_discard(Kart " + idKart + "): Discarded a Message");
    GUI::discardStaticMessage();
}

// ============= DEBUG TESTS ==============
void debug_squash()
{
    int idKart = 0;
    Utils::logInfo("Testing squash");
    Kart::squash(idKart, 5.0);
}

void debug_set_velocity()
{
    int idKart = 0;
    Utils::logInfo("Testing setVelocity");
    Kart::setVelocity(idKart, Vec3(0, 10, 0));
}

// TODO: teleport doesn't work very well
void debug_teleport()
{
    int idKart = 0;
    Utils::logInfo("Testing getLocation + teleport");
    Vec3 loc = Kart::getLocation(idKart);
    Utils::logInfo(Utils::insertValues("Kart %s location : %s %s %s", idKart + "", loc.getX() + "", loc.getY() + "", loc.getZ() + ""));
    Kart::teleport(idKart, Vec3(loc.getX() - 3, loc.getY(), loc.getZ() - 3));
}
