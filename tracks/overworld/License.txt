Israel Ramat Gan ( Area Around My House )

(c) J.Y.Amihud ( Blender Dumbass ) 2023

Under Either: CC-BY-SA 3 or any later version
Or          : GNU  GPL 2 or any later version

Version 2: Updated the asphalt texture.
           Touched the drivelines in a few places
             to reduce AI stupidity.
             
Version 3: Fixed a texture in one place.
           Added Egg hunt mode.
           Added secret area.
